public with sharing class App_TODOs extends fflib_SObjectDomain implements IDomain {
	
	public interface IDomain {
		void markAllComplete();
	}


	public static IDomain newInstance(List<TODO__c> records){
		return (IDomain) App_Application.Domain.newInstance(records);
	}


	public App_TODOs(List<TODO__c> records){
		super(records);
	}


	public void markAllComplete(){
		for(TODO__c record : (List<TODO__c>) records){
			record.Complete__c = true;
		}
	}


	public class Constructor implements fflib_SObjectDomain.IConstructable {
		public fflib_SObjectDomain construct(List<SObject> records){
			return new App_TODOs(records);
		}
	}

}