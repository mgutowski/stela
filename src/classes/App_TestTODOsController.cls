@isTest
private class App_TestTODOsController {
	
	@isTest
	private static void shouldLoadRecordsCorrectly(){

		// Create mocks
		fflib_ApexMocks mocks = new fflib_ApexMocks();
		App_TODOsSelector.ISelector selectorMock = new App_Mocks.MockTODOsSelector(mocks);

		// Given
		mocks.startStubbing();
		
		List<TODO__c> records = new List<TODO__c>{
			new TODO__c(Subject__c = 'hello'),
			new TODO__c(Subject__c = 'world')
		};

		mocks.when(selectorMock.sObjectType()).thenReturn(TODO__c.SObjectType);
		mocks.when(selectorMock.selectAll()).thenReturn(records);
		mocks.stopStubbing();

		// Set mocks
		App_Application.selector.setMock(selectorMock);

		// When
		App_TODOsController controller = new App_TODOsController();

		// Then
		((App_TODOsSelector.ISelector) mocks.verify(selectorMock)).selectAll();
		
		System.assertEquals(2, controller.getRecords().size(), 'Two records should have been loaded, but wasnt');
	}


	@isTest
	private static void shouldAddBlankRowCorrectly(){
		App_TODOsController controller = new App_TODOsController();
		controller.addBlankRow();
		System.assertEquals(1, controller.getRecords().size(), 'One blank row should have been added');
	}


	@isTest
	private static void shouldMarkAllExistingAsCompleted(){
		// Create mocks
		fflib_ApexMocks mocks = new fflib_ApexMocks();
		App_TODOsService.IService serviceMock = new App_Mocks.MockTODOsService(mocks);

		// Set mocks
		App_Application.service.setMock(App_TODOsService.IService.class, serviceMock);

		// When
		App_TODOsController controller = new App_TODOsController();
		PageReference pageRef = controller.markAllComplete();

		// Then
		((App_TODOsService.IService) mocks.verify(serviceMock)).markAllComplete();
	}


	@isTest
	private static void shouldMarkAllExistingShouldHandleExceptionsGracefully(){
		// Create mocks
		fflib_ApexMocks mocks = new fflib_ApexMocks();
		App_TODOsService.IService serviceMock = new App_Mocks.MockTODOsService(mocks);

		// Set mocks
		App_Application.service.setMock(App_TODOsService.IService.class, serviceMock);

		// Given
		mocks.startStubbing();
		
		List<TODO__c> records = new List<TODO__c>{
			new TODO__c(Subject__c = 'hello'),
			new TODO__c(Subject__c = 'world')
		};

		DmlException dmlEx = new DmlException('ERROR!');

		((App_TODOsService.IService) mocks.doThrowWhen(dmlEx, serviceMock)).markAllComplete();
		mocks.stopStubbing();

		// When
		App_TODOsController controller = new App_TODOsController();
		PageReference pageRef = controller.markAllComplete();

		// Then
		((App_TODOsService.IService) mocks.verify(serviceMock)).markAllComplete();
		System.assertEquals(true, ApexPages.hasMessages());
	}


	@isTest
	private static void shouldUpdateRecordsCorrectly(){
		// Create mocks
		fflib_ApexMocks mocks = new fflib_ApexMocks();
		App_TODOsSelector.ISelector selectorMock = new App_Mocks.MockTODOsSelector(mocks);
		fflib_ISObjectUnitOfWork uowMock = new fflib_SObjectMocks.SObjectUnitOfWork(mocks);

		// Test data
		TODO__c existingRecord = new TODO__c(
			Id = fflib_IDGenerator.generate(TODO__c.sObjectType)
		);
		List<TODO__c> records = new List<TODO__c>{ existingRecord };

		// Given
		mocks.startStubbing();
		mocks.when(selectorMock.sObjectType()).thenReturn(TODO__c.SObjectType);
		mocks.when(selectorMock.selectAll()).thenReturn(records);
		mocks.stopStubbing();

		// Set mocks
		App_Application.selector.setMock(selectorMock);
		App_Application.unitOfWork.setMock(uowMock);

		// When
		App_TODOsController controller = new App_TODOsController();
		controller.addBlankRow();
		TODO__c newRecord = controller.getRecords()[1];
		PageReference pageRef = controller.updateRecords();

		// Then
		((fflib_ISObjectUnitOfWork) mocks.verify(uowMock, 1)).registerNew(newRecord);
		((fflib_ISObjectUnitOfWork) mocks.verify(uowMock, 1)).registerDirty(existingRecord);
		((fflib_ISObjectUnitOfWork) mocks.verify(uowMock, 1)).commitWork();
		
	}


	@isTest
	private static void shouldHandleExceptionsGracefullyWhenUpdatingRecords(){
		// Create mocks
		fflib_ApexMocks mocks = new fflib_ApexMocks();
		fflib_ISObjectUnitOfWork uowMock = new fflib_SObjectMocks.SObjectUnitOfWork(mocks);

		// Given
		mocks.startStubbing();

		DmlException dmlEx = new DmlException('ERROR!');
		((fflib_ISObjectUnitOfWork) mocks.doThrowWhen(dmlEx, uowMock)).commitWork();

		mocks.stopStubbing();

		// Set mocks
		App_Application.unitOfWork.setMock(uowMock);

		// When
		App_TODOsController controller = new App_TODOsController();
		controller.updateRecords();

		// Then
		System.assertEquals(true, ApexPages.hasMessages());
		
	}

}