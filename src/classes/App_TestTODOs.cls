@isTest
private class App_TestTODOs {
	
	@isTest
	private static void shouldMarkAllRecordsAsCompleted(){
		List<TODO__c> records = new List<TODO__c>{
			new TODO__c()
		};

		App_TODOs.IDomain domain = App_TODOs.newInstance(records);
		domain.markAllComplete();

		System.assertEquals(true, records[0].Complete__c, 'Record should have been marked as completed, but wasnt');
	}

}