public class StrategyPatternDemo {

   public static void runDemo(){
      
      String operation = 'OperationAdd';
      Context context = new Context((Strategy)Type.forName(operation).newInstatnce());
      System.debug("10 + 5 = " + context.executeStrategy(10, 5));

      operation = 'OperationSubstract';
      context = new Context((Strategy)Type.forName(operation).newInstatnce());
      System.debug("10 - 5 = " + context.executeStrategy(10, 5));

      operation = 'OperationMultiply';
      context = new Context((Strategy)Type.forName(operation).newInstatnce());
      System.debug("10 * 5 = " + context.executeStrategy(10, 5));
   }
   
}