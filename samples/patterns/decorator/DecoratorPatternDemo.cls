public class DecoratorPatternDemo {

   public static void runDemo(){

      Shape circle = new Circle();

      Shape redCircle = new RedShapeDecorator(new Circle());

      Shape redRectangle = new RedShapeDecorator(new Rectangle());
      System.debug('Circle with normal border');
      circle.draw();

      System.debug('Circle of red border');
      redCircle.draw();

      System.debug('Rectangle of red border');
      redRectangle.draw();
   }

}


// Output
// Circle with normal border
// Shape: Circle
// 
// Circle of red border
// Shape: Circle
// Border Color: Red
// 
// Rectangle of red border
// Shape: Rectangle
// Border Color: Red