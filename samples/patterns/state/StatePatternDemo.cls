public class StatePatternDemo {

   public static void runDemo(){
      Context context = new Context();

      StartState startState = new StartState();
      startState.doAction(context);

      System.debug(context.getState().toString());

      StopState stopState = new StopState();
      stopState.doAction(context);

      System.debug(context.getState().toString());
   }

}

// Output
// 
// Player is in start state
// Start State
// Player is in stop state
// Stop State